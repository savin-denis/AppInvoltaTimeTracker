//
//  FormViewController.swift
//  InvoltaTimeTracker
//
//  Created by Денис Савин on 26.03.16.
//  Copyright © 2016 Vladimir Kontsov. All rights reserved.
//

import UIKit
import Eureka
import KeychainSwift
import SwiftyJSON
import Alamofire

class FormController: FormViewController {
    
    let keychain = KeychainSwift(keyPrefix: "AppTimeTracker_")
    var projectId = ""
    var taskId = ""
    var arrayProject: [String] = ["Выбрать"]
    var arrayProjectsId: [String: String] = ["":""]
    var arrayTypeTask: [String] = []
    var arrayTypeTasksId: [String: String] = ["":""]
    var arrayTask: [String] = []
    var arrayTasksId: [String: String] = ["":""]
    var apiDomain = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Новая задача"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor(red:0.97, green:0.87, blue:0.01, alpha:1.0)
       
    
        if let path = NSBundle.mainBundle().pathForResource("config", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> {
                apiDomain = dict["apiDomain"]! as! String
            }
        }
        
        let parameters = [
            "token": String(keychain.get("tokenApp")!),
            ]
        
        Alamofire.request(.GET, "\(apiDomain)/projects", parameters: parameters)
            .responseJSON { response in
                
                let obj: AnyObject = response.data!
                let json = JSON(data: obj as! NSData)
                
                for (_,value) in json {
                    self.arrayProject.insert("\(value["name"])", atIndex: 1)
                    self.arrayProjectsId["\(value["name"])"] = "\(value["id"])"
                }
                if String(response.result) == "SUCCESS" {
                    self.initializeForm()
                }
                
                
        }
        
        
        
       
    }
    
    override func viewDidAppear(animated: Bool) {
    
    }
    
    private func initializeForm() {
        
        form +++=
            Section("Новая или существующая:")
            <<< SegmentedRow<String>("segments"){
                $0.options = ["Новая", "Существующая"]
                $0.value = "Существующая"
        }
        form +++= Section(){
            $0.tag = "new_task"
            $0.hidden = "$segments != 'Новая'" // .Predicate(NSPredicate(format: "$segments != 'Sport'"))
            }
            <<< TextRow(){
                $0.title = "Задача:"
        }
        
        form +++=
            
            //PushRow<RepeatInterval>("Проект") {
            PushRow<String>("Проект") {
                
                $0.title = $0.tag
                $0.options = self.arrayProject
                $0.value = self.arrayProject[0]
                
                }.onChange { [weak self] row in
                    var projectName = row.value!
                    
                    var parameters = [
                        "id": self!.arrayProjectsId[projectName]! as String,
                        "token": self!.keychain.get("tokenApp")! as String,
                    ]
                
                    
                    Alamofire.request(.GET, "\(self!.apiDomain)/tasks", parameters: parameters)
                        .responseJSON { response in
                            
                            let obj: AnyObject = response.data!
                            let json = JSON(data: obj as! NSData)
                            
                            
                            if String(response.result) == "SUCCESS" {
                                for (index,value) in json {
                                    self!.arrayTypeTask.insert("\(value["name"])", atIndex: 0)
                                    self!.arrayTypeTasksId["\(value["name"])"] = "\(value["id"])"
                                    
//                                    self!.arrayTask[Int(index)!] = value["tasks"]
                                    
//                                    for (index,value) in json[Int(index)!]["tasks"] {
//                                        print(value)
//                                        self!.arrayTask.insert("\(value["name"])", atIndex: 0)
//                                        self!.arrayTasksId["\(value["name"])"] = "\(value["id"])"
//                                    }
                                }
                                
                                guard let _ : PushRow<String> = self?.form.rowByTag("Тип задачи") else {
                                    let second = PushRow<String>("Тип задачи") {
                                        $0.title = $0.tag
                                        $0.options = self!.arrayTypeTask
                                        $0.value = "Выбрать"
                                        }.onChange { [weak self] row in
                                        
                                        
                                            
                                            guard let _ : PushRow<String> = self?.form.rowByTag("Задача") else {
                                                let second = PushRow<String>("Задача") {
                                                    $0.hidden = "$segments == 'Новая'"
                                                    $0.title = $0.tag
                                                    $0.options = self!.arrayTask
                                                    $0.value = "Выбрать"
                                                    }.onChange { [weak self] row in
                                                        print(row.indexPath())
                                                        
                                                }
                                                row.section?.insert(second, atIndex: row.indexPath()!.row + 1)
                                                return
                                                
                                            }
                                    }
                                    row.section?.insert(second, atIndex: row.indexPath()!.row + 1)
                                    return
                                    
                                }
                            }
                    }
        }

        
        
    }
    

    
    
    
    enum RepeatInterval : String, CustomStringConvertible {
        case Never = "Never"
        case Every_Day = "Every Day"
        case Every_Week = "Every Week"
        case Every_2_Weeks = "Every 2 Weeks"
        case Every_Month = "Every Month"
        case Every_Year = "Every Year"
        
        var description : String { return rawValue }
        
        static let allValues = [Never, Every_Day, Every_Week, Every_2_Weeks, Every_Month, Every_Year]
    }
    
    enum EventAlert : String, CustomStringConvertible {
        
        case Never = "None"
        case At_time_of_event = "At time of event"
        case Five_Minutes = "5 minutes before"
        case FifTeen_Minutes = "15 minutes before"
        case Half_Hour = "30 minutes before"
        case One_Hour = "1 hour before"
        case Two_Hour = "2 hours before"
        case One_Day = "1 day before"
        case Two_Days = "2 days before"
        
        var description : String { return rawValue }
        
        static let allValues = [Never, At_time_of_event, Five_Minutes, FifTeen_Minutes, Half_Hour, One_Hour, Two_Hour, One_Day, Two_Days]
    }
    


}
//
//class NativeEventNavigationController: UINavigationController, RowControllerType {
//    var completionCallback : ((UIViewController) -> ())?
//}
