//
//  LoginViewController.swift
//  InvoltaTimeTracker
//
//  Created by Денис Савин on 26.03.16.
//  Copyright © 2016 Vladimir Kontsov. All rights reserved.
//

import UIKit
import Foundation
import KeychainSwift
import Alamofire
import SwiftyJSON

class LoginViewController: UIViewController {

    let keychain = KeychainSwift(keyPrefix: "AppTimeTracker_")
    
    @IBOutlet weak var loginFields: UITextField!
    @IBOutlet weak var passFields: UITextField!
    @IBOutlet weak var joinButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.joinButton.setTitle("Войти в систему", forState: .Normal)
        self.loginFields.attributedPlaceholder = NSAttributedString(string: "Логин", attributes: [NSForegroundColorAttributeName: UIColor(red:0.97, green:0.87, blue:0.01, alpha:1.0)])
        self.passFields.attributedPlaceholder = NSAttributedString(string: "Пароль", attributes: [NSForegroundColorAttributeName: UIColor(red:0.97, green:0.87, blue:0.01, alpha:1.0)])
        
        self.errorLabel.hidden = true
    
    }
    
    override func viewDidLayoutSubviews() {
        let borderLogin = CALayer()
        let width = CGFloat(1.0)
        borderLogin.borderColor = UIColor(red:0.97, green:0.87, blue:0.01, alpha:1.0).CGColor
        borderLogin.frame = CGRect(x: 0, y: loginFields.frame.size.height - width, width:  loginFields.frame.size.width, height: loginFields.frame.size.height)
        borderLogin.borderWidth = width
        loginFields.layer.addSublayer(borderLogin)
        loginFields.layer.masksToBounds = true
        
        let borderPass = CALayer()
        borderPass.borderColor = UIColor(red:0.97, green:0.87, blue:0.01, alpha:1.0).CGColor
        borderPass.frame = CGRect(x: 0, y: passFields.frame.size.height - width, width:  passFields.frame.size.width, height: passFields.frame.size.height)
        
        borderPass.borderWidth = width
        passFields.layer.addSublayer(borderPass)
        passFields.layer.masksToBounds = true
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func Join(sender: AnyObject) {
        let login = self.loginFields.text!
        let pass = self.passFields.text!
        if login == "" {
            self.errorLabel.hidden = false
            self.errorLabel.text = "Введите логин"
        } else if pass == "" {
            self.errorLabel.hidden = false
            self.errorLabel.text = "Введите пароль"
        } else {
            self.login(login, pass: pass)
        }
        
        
    }
    
    
    func login(login: String, pass: String) {

        
        var apiDomain = ""
        if let path = NSBundle.mainBundle().pathForResource("config", ofType: "plist") {
            if let dict = NSDictionary(contentsOfFile: path) as? Dictionary<String, AnyObject> {
                apiDomain = dict["apiDomain"]! as! String
            }
        }
        
        let parameters = [
            "username":login,
            "password": pass
        ]
        
        Alamofire.request(.POST, "\(apiDomain)/auth/login", parameters: parameters)
            .responseJSON { response in
                
                var statusLogin: Int

                let obj: AnyObject = response.data!
                let json = JSON(data: obj as! NSData)
                
                statusLogin = json["status"].int!
                
                    switch statusLogin {
                    case 1:
                        print("Success")
                        self.keychain.set(json["token"].string!, forKey: "tokenApp")
                        self.keychain.set(String(json["id"].int!), forKey: "userId")
                        self.performSegueWithIdentifier("LoginAppController", sender: self)
                    case 0:
                        let errors = json["errors"]["password"].first?.1
                        self.errorLabel.hidden = false
                        self.errorLabel.text = errors?.string!
                    default:
                        print(response)
                        break
                    }
        }
    }
    
    
    
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

}
